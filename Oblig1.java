/*
 * Oblig 1 INF1010
 * stiako, mathiapk
 * Stian Kongsvik og Mathias Kallstrom
 * gruppe 8, gruppe 9
 */

//main. Kaller paa klassen datastruktur
class Oblig1{
    public static void main(String[] args){
	Datastruktur d = new Datastruktur();
    }
}
//Denne klassen oppretter personobjektene og skriver datastrukturen.
class Datastruktur {
    Person jeg = new Person("Jeg", 3);
    Person ask = new Person("Ask", 3);
    Person dana = new Person("Dana", 3);
    Person tom = new Person("Tom", 3);
    
    Datastruktur(){
	
	//Skriver relasjoner for "jeg"
	jeg.blirKjentMed(ask);
	jeg.blirKjentMed(dana);
	jeg.blirKjentMed(tom);

	jeg.blirVennMed(ask);
	jeg.blirVennMed(dana);
	jeg.blirVennMed(tom);
	
	//Skriver relasjoner for "ask"
	ask.blirKjentMed(jeg);
	ask.blirKjentMed(tom);
	ask.blirKjentMed(dana);
	
	ask.blirForelsketI(jeg);
	ask.blirUvennMed(dana);
	ask.blirUvennMed(tom);

	//Skriver relasjoner for dana
	dana.blirKjentMed(ask);
	dana.blirKjentMed(tom);
	dana.blirKjentMed(jeg);

	dana.blirForelsketI(tom);
	dana.blirUvennMed(jeg);

	//Skriver relasjoner for tom
	tom.blirKjentMed(jeg);
	tom.blirKjentMed(ask);
	tom.blirKjentMed(dana);
	
	tom.blirForelsketI(dana);
	tom.blirUvennMed(ask);
	tom.blirUvennMed(jeg);

	//Skriver ut alt om alle
	jeg.skrivUtAltOmMeg();
	ask.skrivUtAltOmMeg();
	dana.skrivUtAltOmMeg();
	tom.skrivUtAltOmMeg();
    }
}
//Klassen "Person" inneholder alle metoder for aa knytte personobjektene sammen. 
class Person{
    private String navn;  
    private Person[] kjenner;
    private Person[] likerikke;
    // likerikke = uvenner 
    // venner blir da alle personer som pekes paa av kjenner 
    // unntatt personen(e) som pekes paa av likerikke
    private Person forelsketi;
    private Person sammenmed;
    //forelsketi og sammenmed peker paa eventuell romanse kjaereste. Peker altsaa paa et personobjekt

    //Konstruktor for klassen Person. Her settes navn og lengde for arrayene "kjenner" og "likerikke"
    Person(String navn, int lengde){
	this.navn = navn;
	kjenner = new Person[lengde];
	likerikke = new Person[lengde];
    }
    //Denne metoden brukes kun i utskrift, for aa hente navn til de ulike personobjektene
    public String hentNavn(){
    	return navn;
    }
    //Metode som sjekker om to personobjekter kjenner hverandre. Returnerer en boolsk verdi.
    public boolean erKjentMed(Person p){
	for(int i = 0;i<p.kjenner.length; i++){
	    if(kjenner[i] == p){
		return true;
	    }
	}
	return false;
    }
    //Metode som sjekker og eventuelt legger til bekjentskaper mellom personobjektene. 
    //Personobjektene legges da til i "kjenner-arrayet".
    public void blirKjentMed(Person p){
	if(navn.equals(p.navn)){
	    System.out.println("Kan ikke bli kjent med seg selv");
	} else if(erKjentMed(p)){
	    System.out.println("Kjenner allerede denne personen");
	}else {
	    for(int i = 0;i<kjenner.length;i++){
		if(kjenner[i] == null){
		    kjenner[i] = p;
		    break;
		}
	    }
	}
    }
    //Gir et objekt til pekeren "forelsketi". Sjekker ogsaa om instansen er det samme som objektet som blir sjekket.
    public void blirForelsketI(Person p) {
	if(this != p){
	    forelsketi = p;
	} else {
	    System.out.println("Kan ikke vaere forelsket i seg selv");
	}  
    }
    //Samme som "blirKjentMed", men her legges objektet inn i "likerikke-arrayet".
    //Sjekker ogsaa om objektet allerede ligger i arrayet.
    public void blirUvennMed(Person p){
	boolean finnes = true;
	if(navn.equals(p.navn)){
	    System.out.println("Kan ikke bli uvenn med seg selv");
	}else{
	    for(int i = 0;i<likerikke.length;i++){
		if(likerikke[i] == p){
		    System.out.println("Er allerede uvenn med denne personen");
		}else{
		    finnes = false;
		}
	    }
	}
	//Her legges personobjektet til i "likerikke-arrayet", dersom det ikke allerede er der.
	if(!finnes){
	    for(int i = 0;i<likerikke.length;i++){
		if(likerikke[i] == null) {
		    likerikke[i] = p;
		    break;
		}
	    }
	}
    }
    //Sjekker om et personobjeket er venn med et annet.
    //Sjekker forst om de kjenner hverandre, via "kjenner-arrayet".
    //Deretter sjekkes "likerikke-arrayet". Dersom personobjektet ligger i sistnevnte kan de ikke vaere venner.
    public boolean erVennMed(Person p){
	boolean venn = false;
	for(int i = 0;i<p.kjenner.length;i++){
	    if(kjenner[i] == p){
		venn = true;
	    }
	    if(likerikke[i] == p){
		venn = false;
	    }
	}
	return venn;
    }
    //Sjekker metoden "erVennMed". Dersom de allerede er venner kan de ikke bli det paa nytt.
    //Sjekker deretter "likerikke-arrayet". 
    //Dersom personobjektet finnes der blir plassen i arrayet satt til null
    public void blirVennMed(Person p){
	if(!erVennMed(p)){
	    for(int i = 0;i<likerikke.length;i++){
		if(likerikke[i] == p){
		    likerikke[i] = null;
		}
	    }
	}
    }
    //Metode som skriver ut vennene for et bestemt personobjekt, fra metoden "hentVenner".
    public void skrivUtVenner(){
	System.out.println(hentVenner());
    }
    //Henter bestevenn som blir returnert som en string.
    //Bestevenn er for enkelhetens skyld den kjenningen som ligger paa plass "0" i kjenner-arrayet.
    public String hentBestevenner(){
	return kjenner[0].navn;
    }
    //henter venner fra metoden "erVennMed". Legger til nanvene i en String og returnerer denne Stringen.
    public String hentVenner(){
	String venner = "";
	for(int i = 0;i<kjenner.length;i++){
	    if(erVennMed(kjenner[i])){
		venner += kjenner[i].navn + " ";
	    }
	}
	return venner;
    }
    //Returnerer antall venner for et personobjekt
    //Sjekker metoden "erVennMed" og plusser "int antall" per venn.
    public int antVenner(){
	int antall = 0;
	for(int i = 0;i<kjenner.length;i++){
	    if(erVennMed(kjenner[i]))
		antall++;
	}
	return antall;
    }
    //Skriver ut alle navn til objekter som befinner seg i "kjenner-arrayet".
    public void skrivUtKjenninger(){
	for (Person p: kjenner){
	    if (p!=null){
		System.out.print(p.hentNavn() + " ");
	    }
	}
	System.out.println("");
    }
    //Samme som metoden over, men for "likerikke-arrayet".
    public void skrivUtLikerIkke(){
	for (Person p: likerikke) {
	    if (p!=null){
		System.out.print(p.hentNavn() + " ");
	    }
	}
	System.out.println("");
    }
    //Her hentes og samles all informasjon om personobjektet, fOr det skrives ut. 
    //Denne metoden bruker "skrivUtKjenninger" og "skrivUtLikerIkke".
    //Denne metoden skriver altsaa ut alle relasjoner for et personobjekt. 
    public void skrivUtAltOmMeg(){
	boolean mislikerNoen = false;
	System.out.print(navn + " kjenner: ");
	skrivUtKjenninger();
	if(forelsketi!= null){
	    System.out.println(navn + " er forelsket i " + forelsketi.hentNavn());
	}
	for(int i = 0;i<likerikke.length;i++) {
	    if(likerikke[i] != null){
		mislikerNoen = true;
	    }
	}
	if(mislikerNoen){
	    System.out.print(navn + " liker ikke: ");
	    skrivUtLikerIkke();
	}
    }
}
